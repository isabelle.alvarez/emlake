/*! \file lake_Bourget_C1T31bql_sc.h
 *
 *  \author: I. Alvarez, INRAE
 *  \brief  Fichier contant la d�finition d'un mod�le de viabilit� pour les tests
 *
 *  PB DU LAC paramètres de Brias et al
 *  CONTRAINTES 1 équilibre mésotrophique
 *  Avec sygmoïde et classique
 *  tyche 3 sur b [2.2,2.3] et q [2.2,2.3] et l [1/19,1/16]
 *  2 modèles
 *  
 */

// #include <math.h>
#ifndef TESTDATA_H_
#define TESTDATA_H_


/***********************************************************
 * Some model specific parameters can be defined here
 *************************************************************/
int axisAcc = 1000;

/*!
 * \var dbFileName
 * the  name for the  main data base file for the project
 */

string prefix="bourget_C1T31bql_sc" ;

/*******************************
volume v = 3.6
input moyen en tonnes Lmoy = 33.81538 (à diviser par v)
# en tonnes
s = 2.1476;
h = 0.12;
r = 367.04;
q = 2.222;
m = 96.85;
b = s + h; 2.2676
r_v = r / v [1] 101.9556
m_v = m / v [1] 26.90278

deltaL_max = 22.7/v #variation max de l'input sur 12 ans
contrôle : VLmax=0.5*deltaL_max

équilibres :
limite oligotrophe
Le_1 = L0_1 / v
#[1] 12.67958
Pe_1 = P0_1 /v
#[1] 11.65333

limite mesotrophe
 Le_2 = L0_2 / v
 #[1] 9.855537
 Pe_2 = P0_2 /v
 Pe_2
 #[1] 24.76444

L_min arbitraire, un peu en dessous de la moyenne : 25.0 /v
max pour le calcul 80/v (au-dessus du max enregistré)

*********************************/

double volume_lac = 3.6 ;
double Lmoy_v = 33.81538 /volume_lac ;
double deltaL_max = 22.7 /volume_lac;
double umax = deltaL_max ; 
double umin = -0.5*deltaL_max ; // c'est plus dur de réduire
// double umax = 0.5*deltaL_max ; 

double tau = 0.1; //tau = 0.01 ; paramètre de discrétisation du temps
double m_lac = 96.85 /volume_lac ;
double q = 2.222 ; // 
/*double q_min = 2.22 ; // */
/*double q_max = 2.23 ; // */
double q_min = 2.2 ; // 
double q_max = 2.3 ; // 

/*double b_min = 2.25;*/
/*double b_max = 2.27;*/
double b_min = 2.2;
double b_max = 2.3;
//double b = 2.2676;
double r_min = 100.0; // déjà divisé par v
double r_max = 102.0;
double r = 101.9556 ;

double alpha_min = 0.0;
/*double alpha_max = 0.0;*/
double alpha_max = 1.0;

double lambda = 1/16.0;
double lambda_max = 1/16.0;
double lambda_min = 1/19.0;

/*double CONTROL_MIN_ty[dimc_ty]={0.5,0.6,0.0}; // b,r,alpha*/
/*double CONTROL_MAX_ty[dimc_ty]={0.7,1.0,0.0}; // b,r,alpha
*/
// ATTENTION l_max est une variable interdite dans le code d'Anya

double L_min = 25.0/volume_lac; //arbitraire, un peu en-dessous de la moyenne
// double L_max = 80.0/v;
double L_max = 100.0/volume_lac;
double P_min = 0.0;
double P_max = 24.76;  // ordonnée du de l'équilibre mésotrophique

////////////////////////////////////
/// variable d'états
////////////////////////////////////
/*! \var dim
 *  \brief State  dimension
 */
const int dim=2;

/*! \var dicret_type
 *  \brief the  type of discretization scheme used  for the dynamics
 *  EE or 1 = Euler Explicit  scheme
 *  EI or 2 = Euler Implicit
 *  RK2I or 3 = RK2 Implicit (RK2 for -F)
 *  RK2E or 4 = RK2 Explicit (RK2 for F)
 */
const int discret_type=0;

/*!
 * \var dynType  defines the type of dynamics in the model
 *      1 or CC : continuous in time and space
 *      2 or DC : discrete time continuous space
 *      2 or DD : discrete time discrete space
 *      4 or HD : hybrid TODO
 */
const int dynType=DD;


/*! \var STATE_MIN[dim]
 *  \brief min values  for  state vector components
 *   \see gridParams
 */
double STATE_MIN[dim]={L_min,P_min}; // L,P


/*! \var STATE_MAX[dim]
 *  \brief max values  for  state vector components
 *   \see gridParams
 */
double STATE_MAX[dim]={L_max,P_max}; // L,P


/*!
 * \var nbPointsState[dim]
 * number of  discretization points for the state variable
 * \see gridParams
 */
unsigned long long int nbPointsState[dim] = {axisAcc,axisAcc};


double pas_0 = double( (STATE_MAX[0] - STATE_MIN[0]) / (nbPointsState[0]-1))  ;
double pas_1 = double( (STATE_MAX[1] - STATE_MIN[1]) / (nbPointsState[1]-1))  ;


/*      *****************************************
 *  Definition of constraints and target
 *************************************************** */

/*!
 * \brief Function  defining the state   constraints, corresonds  to k(x)
 *
 * This function defines the set K for admissible  states
 * @param x state variable
 * @return  value that caraterise the constraints set
 */


inline double  constraintsX_fd( unsigned long long int * x )
{
/*  double xReel0=STATE_MIN[0]+x[0]* pas_0;
  double xReel1=STATE_MIN[1]+x[1]* pas_1;
  double norme_x = sqrt(xReel0*xReel0 + xReel1*xReel1);
  double res = 1.0;

  if(norme_x <= min(2*alpha,r) ) {res = 1.0;} else {res = PLUS_INF;};

  return res;*/
    return 1.0;
}


////////////////////////////////////
///    controles 
////////////////////////////////////

/*! \var dimc
 *  \brief Control variable   dimension
 */
const int dimc=1;

/*! \var CONTROL_MIN[dimc]
 *  \brief minimum values  for  control vector components
 *   \see controlParams
 */
double CONTROL_MIN[dimc]={umin};

/*! \var CONTROL_MAX[dimc]
 *  \brief maximum values  for  control vector components
 *   \see controlParams
 */
double CONTROL_MAX[dimc]={umax};

/*! 
 * \var nbPointsControl[dimc]
 * number of  discretization points for the control variable
 * \see controlParams
 */
unsigned long long int nbPointsControl[dimc] =  {11};


double pasC_0 = double( (CONTROL_MAX[0] - CONTROL_MIN[0]) / (nbPointsControl[0]-1))  ;
//double pasC_1 = double( (CONTROL_MAX[1] - CONTROL_MIN[1]) / (nbPointsControl[1]-1))  ;

/*!
 * \brief Function  defining the mixed  constraints
 *
 * This function defines the set U(x) for admissible controls as function of the state
 * @param x state variable
 * @param u control variable
 * @return  value that caraterise the constraints set
 */


inline double  constraintsXU_fd( unsigned long long int * x, unsigned long long int * u )
{

 /* double xReel0=STATE_MIN[0]+x[0]* pas_0;
  double xReel1=STATE_MIN[1]+x[1]* pas_1;
  double norme_x = sqrt(xReel0*xReel0 + xReel1*xReel1);

  double uReel0=CONTROL_MIN[0]+u[0]* pasC_0;
  double uReel1=CONTROL_MIN[1]+u[1]* pasC_1;
  double norme_u = sqrt(uReel0*uReel0 + uReel1*uReel1);

  double res = 1.0;

  if ( norme_u > alpha ) {res = PLUS_INF;} else {res = 1.0;};

  return res;*/
    return 1.0;
}


////////////////////////////////////
///    tychastique 
////////////////////////////////////

// tyches : alpha entre : 0 et 1

//const int dimc_ty=1;
const int dimc_ty=4;

//************************* changer ces valeurs

// double CONTROL_MIN_ty[dimc_ty]={b_min, q_min, r_min}; // b,q,r
// double CONTROL_MIN_ty[dimc_ty]={b_min}; // b
// double CONTROL_MIN_ty[dimc_ty]={b_min, lambda_min}; // b,lambda
//double CONTROL_MIN_ty[dimc_ty]={b_min, lambda_min, alpha_min}; // b,lambda, alpha
//double CONTROL_MIN_ty[dimc_ty]={b_min, alpha_min}; // b, alpha
//double CONTROL_MIN_ty[dimc_ty]={b_min, alpha_min, q_min}; // b, alpha, q
double CONTROL_MIN_ty[dimc_ty]={b_min, alpha_min, q_min, lambda_min}; // b, alpha, q, lambda
// double CONTROL_MIN_ty[dimc_ty]={b_min,r_min,q_min}; // b,r,q
// double CONTROL_MIN_ty[dimc_ty]={b_min,r_min,alpha_min}; // b,r,alpha
/*! \var CONTROL_MAX_ty[dimc]
 *  \brief maximum values  for  tyches vector components
 *   \see controlParams
 */
//double CONTROL_MAX_ty[dimc_ty]={b_max,q_max,r_max}; // b,q, r
//double CONTROL_MAX_ty[dimc_ty]={b_max,r_max,alpha_max}; // b,r,alpha
//double CONTROL_MAX_ty[dimc_ty]={b_max, lambda_max}; // b,lambda
//double CONTROL_MAX_ty[dimc_ty]={b_max, lambda_max, alpha_max}; // b,lambda, alpha
//double CONTROL_MAX_ty[dimc_ty]={b_max, alpha_max}; // b, alpha
double CONTROL_MAX_ty[dimc_ty]={b_max, alpha_max,q_max, lambda_max}; // b, alpha, q, lambda

//************************* 

unsigned long long nbPointsControl_ty[dimc_ty]={5,11,5,5};
// unsigned long long nbPointsControl_ty[dimc_ty]={5,5,5};
// unsigned long long nbPointsControl_ty[dimc_ty]={10,10};
// unsigned long long nbPointsControl_ty[dimc_ty]={10,10,11};
/*unsigned long long nbPointsControl_ty[dimc_ty]={10,10,10};*/



double  pas_ty_0 = ( (CONTROL_MAX_ty[0] - CONTROL_MIN_ty[0]) / (nbPointsControl_ty[0]-1))  ;
double  pas_ty_1 = ( (CONTROL_MAX_ty[1] - CONTROL_MIN_ty[1]) / (nbPointsControl_ty[1]-1))  ;
double  pas_ty_2 = ( (CONTROL_MAX_ty[2] - CONTROL_MIN_ty[2]) / (nbPointsControl_ty[2]-1))  ;
double  pas_ty_3 = ( (CONTROL_MAX_ty[3] - CONTROL_MIN_ty[3]) / (nbPointsControl_ty[3]-1))  ;

////////////////////////////////////
///    param viablab 
////////////////////////////////////

/*!
 * \var T maximum time horizon for the study
 */
double T= 20000;
// ne sert pas ?


/*
 * Direction le long de laquelle l'ensemble sera représenté par des suites de bits
 */
unsigned long long int dirTramage =1;

/*
 * Sélection de la méthode de représentation de l'ensemble
 * Ce paramètre détermine quelle classe sera utilisée pour les calculs
 *
 *    - BS = BitSet, représentation par fonction caractéristique , pour
 *                   tout type de calculs
 *    - MM = MicroMacro, représentation par valeurs réelles, pour les calculs
 *          d'ensembles épigraphiques , associés aux système micro-macro
 */
int gridMethod=BS;

/*
 * Sélection de l'ensemble à calculer
 */
int setType=VIAB;

/*
 * Nombre de raffinements successifs dans le calcul
 */
int refine= 0; 

/*!
 * \var periodic[dim]
 * \brief indicator of periodicity
 * 1= periodic variable
 * 0=non periodic variable
 * \see gridParams
 */
int periodic[dim]={0};


/*
 * Definition of the dynamics  and associated functions and constants
 */

/*
 * Paramètre qui indique si l'ensemble doit être calculé
 * dans le cas contrainte il devra pouvoir être chargé depuis un fichier pour une étude de
 * trajectoires
 */
int computeSet=1;

int saveBoundary=0;

const int nbTrajs=0;
double initPoints[dim*nbTrajs]={};

//**************** c'est pour dire où on a le droit de sortir
int sortieOKinf[dim]={0,0};
int sortieOKsup[dim]={0,0};


void loadModelData() {};


// ///////////////////////////////////
//Fonctions utiles pour la dynamique
// //////////////////////////////////
// double sediment_p(double p) {return p*p*p*p*p*p*p*p/(m_lac*m_lac*m_lac*m_lac*m_lac*m_lac*m_lac*m_lac + p*p*p*p*p*p*p*p) ;} ;

double sediment_p(double p, double le_q) {return pow(p,le_q)/(pow(m_lac,le_q) + pow(p,le_q)) ;} ;

double sediment_s(double p, double le_lambda) {return p/(p + m_lac * exp(-1 * le_lambda *(p - m_lac))) ;} ;


////////////////////////////////////
///    fonction Dynamique 
////////////////////////////////////



void dynamics_tych_fd(unsigned long long int  * x, unsigned long long int *u,unsigned long long int *v, unsigned long long int * image)
{

  double xReel0=STATE_MIN[0]+x[0]* pas_0;
  double xReel1=STATE_MIN[1]+x[1]* pas_1;

// c'est quoi ça ?  double c=STATE_MIN[2]+x[2]* pas_2;
//  double norme_x = sqrt(xReel0*xReel0 + xReel1*xReel1 + xReel2*xReel2);
  double norme_x = sqrt(xReel0*xReel0 + xReel1*xReel1);

  double uReel0=CONTROL_MIN[0]+u[0]* pasC_0;
//  double uReel1=CONTROL_MIN[1]+u[1]* pasC_1;
//  double norme_u = sqrt(uReel0*uReel0 + uReel1*uReel1);


  double vReel0=CONTROL_MIN_ty[0]+v[0]* pas_ty_0; //b
  double vReel1=CONTROL_MIN_ty[1]+v[1]* pas_ty_1; //alpha
  double vReel2=CONTROL_MIN_ty[2]+v[2]* pas_ty_2; //q
  double vReel3=CONTROL_MIN_ty[3]+v[3]* pas_ty_3; //lambda

//  double vReel2=CONTROL_MIN_ty[2]+v[2]* pas_ty_2; //r
//  double vReel2=CONTROL_MIN_ty[2]+v[2]* pas_ty_2; //alpha rajoute une expression  alpha*sigmoid à la dynamique de P

  //double temp_im0 =  uReel0   ;
  //double temp_im1 =  uReel1   ;

 // double temp_im0 = 2*xReel0 + uReel0 + vReel0  ;
 // double temp_im1 = 2*xReel1 + uReel1 + vReel1  ;


    double temp_im0 = xReel0 + tau * uReel0;
//    double temp_im1 = xReel1 + tau * (-vReel0*xReel1 + xReel0 ) + tau *  (1-vReel2) * vReel1 * sediment_p(xReel1) + tau*vReel2*c_alpha*sqrt(xReel1) ;
//  double temp_im1 = xReel1 + tau * (-vReel0*xReel1 + xReel0 ) + tau *  (1-vReel2) * vReel1 * sediment_p(xReel1) ;
// P_t+1 = P_t + tau (-bP + L + r (P/(m^q + P^q)))
//  double temp_im1 = xReel1 + tau * (-vReel0*xReel1 + xReel0 ) + tau *  vReel1 * sediment_p(xReel1) ;
//  double temp_im1 = xReel1 + tau * (-vReel0*xReel1 + xReel0 ) + tau *  r * sediment_p(xReel1,q) ;
//  double temp_im1 = xReel1 + tau * (-vReel0*xReel1 + xReel0 ) + tau *  r * sediment_p(xReel1,vReel1) ;
// double temp_im1 = xReel1 + tau * (-vReel0*xReel1 + xReel0 ) + tau *  r * sediment_s(xReel1,vReel1) ;

//  double temp_im1 = xReel1 + tau * (-vReel0*xReel1 + xReel0 ) + tau *  r * vReel1 * sediment_s(xReel1,lambda) + tau * r * (1.0- vReel1) * sediment_p(xReel1,q) ;

  double temp_im1 = xReel1 + tau * (-vReel0*xReel1 + xReel0 ) + tau *  r * vReel1 * sediment_s(xReel1,vReel3) + tau * r * (1.0- vReel1) * sediment_p(xReel1,vReel2) ;


  //////// projections

  // L
  unsigned long long int temp_k0 = floor( (temp_im0 - STATE_MIN[0]) / pas_0) ;

  if (  abs( temp_im0 - (STATE_MIN[0]+ temp_k0* pas_0) ) <  abs( temp_im0 - (STATE_MIN[0]+ (temp_k0+1)* pas_0) )  ){ image[0] = (unsigned long long int) temp_k0 ; } else {  image[0] = (unsigned long long int) temp_k0 +1  ;  } ;

  // P
  unsigned long long int temp_k1 = floor( (temp_im1 - STATE_MIN[1]) / pas_1 ) ;

  if (  abs( temp_im1 - (STATE_MIN[1]+ temp_k1* pas_1) ) <  abs( temp_im1 - (STATE_MIN[1]+ (temp_k1+1)* pas_1) )  ){ image[1] = (unsigned long long int) temp_k1 ;  } else {  image[1] = (unsigned long long int) temp_k1 +1 ; } ;

}



#include "lake_Bourget_unused.h"
#endif /* TESTDATA_H_ */
