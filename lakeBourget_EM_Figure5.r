############################################################################################################
#### Viability and Guaranteed Viability kernels for Lake Bourget (parameters and data from Brias et al, 2018)
############################################################################################################
## DO INIT : run everything until ### FIN ####
## TO PRINT FIGURES GO TO IMPRESSION FIGURES
## run the 2 scenario lines
## select the code of the figure you want to print and run it
#############################################################

# en tonnes
s = 2.1476;
h = 0.12;
r = 367.04;
# r = 400;
q_b = 2.222;
m = 96.85;
b = s + h;

#en concentration
volume = 3.6e9 #en m^3
v = 3.6
r_v = r / v
m_v = m / v
#P_v = P/v
#L_v = L/v

t2mu <- function(x) x/v

name = "Le Bourget"

#conversion de P en tonnes en Pv = P/V en \mu g.L⁻1
#on divise sa valeur par 3.6
# r et m sont en tonnes donc idem on doit les diviser par 3.6
# et on devrait obtenir la même dynamique partout.
# et le discours serait plus facile à tenir, 
# en disant que localement la concentratio de P a augmenté.
col1 = "gray"

# paramètres de calcul
findestemps=1000;
ndiv=10000; # nb de points pour le calcul de l'équilibre

# détermination de la ligne des équilibres

#L est en tons
L_in = c(48,
        28,
        25,
        32.8,
        18.2,
        13.7,
        20.4,
        26.8,
        40.3,
        52.8,
        42.1,
        57.1,
        34.4)

L_moy = mean(L_in)/v;
# > mean(L_in)
# [1] 33.81538
deltaL = L_in[-length(L_in)] - L_in[-1]
# > deltaL
# [1]  20.0   3.0  -7.8  14.6   4.5  -6.7  -6.4 -13.5 -12.5  10.7 -15.0  22.7
deltaL_max = max(max(deltaL,-deltaL))
deltaL_max
#[1] 22.7

#à  donner par l'utilisateur
gymax<-160.0/v; #valeur max considérée pour P sur les graphiques
Lmin<-25.0/v; #arbitraire, un peu en-dessous de la moyenne
Lmax=100.0/v ; #arbitraire, au-dessus du max enregistré

Pdiv<-gymax/ndiv;
Ldiv<-(Lmax-Lmin)/ndiv;

#contrôle
VLmax=0.5*deltaL_max/v
#VLmin=0.5*deltaL_max/v

# init pour le calcul des équilibres en mug.l⁻¹
bool<-1;
Pdiv<-gymax/ndiv;
Ldiv<-(Lmax-Lmin)/ndiv;
P0<-seq(0,gymax,Pdiv);

f <- function(y,le_q=q_b,le_b=b) {le_b*y-r*y^le_q/(m^le_q+y^le_q)}; # en tonnes
f_v <- function(y,le_q=q_b,le_b=b) {le_b*y-r_v*y^le_q/(m_v^le_q+y^le_q)}; # en \mu g.L^-1

L0<-f_v(y=P0);

#Preparation graphique


#plot(L0,P0,pch=" ",xlim=c(0,Lmax),ylim=c(0,1.5),xlab="L",ylab="P",cex.lab=1.2, frame.plot=FALSE, axes= FALSE, xaxs="i", yaxs="i", asp=1, );
#plot(L0,P0,pch=" ",xlim=c(0,Lmax),ylim=c(0,1.5), frame.plot=FALSE, xlab="L",ylab="P", cex.lab=1.2, ann=FALSE, axes= FALSE,  asp=1, );
#plot(L0,P0,pch=" ",xlim=c(0,Lmax),ylim=c(0,gymax), frame.plot=FALSE, xlab="L (µgl⁻¹)",ylab="P (µgl⁻¹)",   xaxs="i", yaxs="i", yaxt="n" );
#axis(1, pos=0)
#axis(2, pos=0)
#leg1<-paste("Pmax=",Pmax);
#leg2<-paste("Lmin=",Lmin);
#text(c(0.2,0.9),c(1.45,1.45),c(leg2,leg1),cex=1.0);
#lines(L0,P0,lty=2)

# ?
# totcase=length(vLmin)*length(vPmax);
# mat <- matrix(1:totcase,length(vLmin),length(vPmax));
# layout(mat);

# valeurs de l'équilibre qui ont une tg verticale
# les P sont croissants.
# les L commencent à 0. On fait L[n]-L[n+1]
deltaL0 <- L0[-length(L0)] - L0[-1]
indice <- (deltaL0 < 0)
indFirst <- match(FALSE,indice)
# > indFirst
# [1] 2623
# > P0[2620:2625]
# [1] 41.904 41.920 41.936 41.952 41.968 41.984
# > L0[2620:2625]
# [1] 45.64645 45.64648 45.64649 45.64649 45.64649 45.64648
# pas d'interpolation, on dit que indFirst est la bonne valeur
# On recherche le second point en virant la première partie de la courbe
P0_1 = P0[indFirst]
L0_1 = L0[indFirst]
# > L0_1*v
#[1] 12.67958 *v [1] 45.64649
# > P0_1*v
# [1] 11.65333 *v [1] 41.952
resteL <- deltaL0[indFirst:length(deltaL0)]
indice2 <- (resteL > 0)
ind2First <- match(FALSE,indice2)
indSecond <- ind2First+indFirst -1
P0_2 = P0[indSecond]
L0_2 = L0[indSecond]
# > L0_2
# [1] 9.855537 *v [1] 35.47993
# > P0_2
# [1] 24.76444 *v [1] 89.152

## courbe inverse depuis (L_e,P_e)
## cas d'une courbe qui ne traverse pas la courbe des équilibres
## Exemple avec P_e qui vaut P0_2
# P_e = P0_2
# donne l'image courbeP0_2.png
#P_e = P0_2 / 1.5

P_e = P0_2
L_e = f_v(P_e)
nbpas = 1000 #nb de pas pour calculer une trajectoire
 
courbeInverse <- function(L_e,P_e,u,nbpas,L_cal,le_q=q_b,le_b=b) { # L_cal = 50 valeur max envisagée pour la trajectoire
  i =1
  LesP <- c(P_e)
  LesL <- seq(from=L_e,to=L_cal,length.out = (nbpas+1))
  precision <- (L_cal - L_e)/nbpas
  while (LesP[length(LesP)]>0 && i <= nbpas) {
    LesP <- c(LesP, LesP[i]+(f_v(LesP[i],le_q=le_q,le_b=le_b)-LesL[i])*precision/u)
    i <- i+1
  } #fin while
  bound <- data.frame(L=LesL[1:length(LesP)],P=LesP)
  # interpolation du dernier point pour avoir P=0
  # L_f= L_t_1 - P_t_1 (L_t - L_t_1)/(P_t -P_t_1)
  taille <- dim(bound)[1]
  if (bound$P[taille]< 0) { #sinon c'est qu'on n'est pas allé jusqu'au bout
    P_f =0
    L_t_1 = bound$L[taille-1]
    L_t = bound$L[taille]
    P_t_1 = bound$P[taille-1]    
    P_t = bound$P[taille]
    L_f = L_t_1 - P_t_1 * (L_t - L_t_1) / (P_t -P_t_1)
    bound$L[taille] <- L_f
    bound$P[taille] <- P_f
  } else {
    print("augmenter le nombre de pas ou la limite en L")
  }
  bound
} #fin fonction

 bound = courbeInverse(L_e,P_e,VLmax,1000,50)
t = which(bound$P==0)
longueur = (if (is.na(t)) dim(bound)[1] else t)
bound = bound[1:longueur,]
#affichage
#lines(bound$L,bound$P,col="red")

Pmax=P_e
# K
lines(c(Lmin,Lmin),c(0,Pmax))
lines(c(Lmin,Lmax),c(Pmax,Pmax))

# impression
eq_v=data.frame(L0=L0,P0=P0)
# sauvegarde
#write.table(eq_v, file=paste0("eq_v","u",VLmax,".csv"),row.names=FALSE, sep=";",dec=".")
#write.table(bound, file=paste0("bound_v","u",VLmax, ".csv"), row.names=FALSE, sep=";",dec=".")

### VARIATION de q
# f_q <- function(y,q=q_b) {b*y-r*y^q/(m^q+y^q)}; # en tonnes
# f_qv <-function(y,q=q_b) {b*y-r_v*y^q/(m_v^q+y^q)}; # en \mu g.L^-1
# L0_q<-f_qv(y=P0,q=2);
# lines(L0_q,P0,lty=2, col="blue")

### Après tous ces essais, on décide de prendre q :
# entre 2.1 (de toute façon ça ne peut pas être inférieur à 2)
# et 2.5 (ou 3 mais là c'est déjà carrément bizarre, et à 5 le lac est irréversible)

######### SIGMOIDE ############
### Pour la sygmoïde en µgl⁻¹

g_v <- function(y,lambda=1,le_b=b) {le_b*y-r_v*y/(y+m_v*exp(-lambda*(y-m_v)))}
P0<-seq(0,gymax,Pdiv);

# L0_s <-g_v(y=P0,lambda=1/17.5);
# lines(L0_s,P0,lty=2, col="orange")

# L0_s <-g_v(y=P0,lambda=1/30);
# lines(L0_s,P0,lty=2, col="green")

courbeInverse_s <- function(L_e,P_e,u,nbpas,L_cal, lambda=1/17.5,le_b=b) { # L_cal = 50 valeur max envisagée pour la trajectoire
  i =1
  LesP <- c(P_e)
  LesL <- seq(from=L_e,to=L_cal,length.out = (nbpas+1))
  precision <- (L_cal - L_e)/nbpas
  while (LesP[length(LesP)]>0 && i <= nbpas) {
    LesP <- c(LesP, LesP[i]+(g_v(LesP[i],lambda=lambda,le_b=le_b)-LesL[i])*precision/u)
    i <- i+1
  } #fin while
  bound <- data.frame(L=LesL[1:length(LesP)],P=LesP)
  # interpolation du dernier point pour avoir P=0
  # L_f= L_t_1 - P_t_1 (L_t - L_t_1)/(P_t -P_t_1)
  taille <- dim(bound)[1]
  if (bound$P[taille]< 0) { #sinon c'est qu'on n'est pas allé jusqu'au bout
    P_f =0
    L_t_1 = bound$L[taille-1]
    L_t = bound$L[taille]
    P_t_1 = bound$P[taille-1]    
    P_t = bound$P[taille]
    L_f = L_t_1 - P_t_1 * (L_t - L_t_1) / (P_t -P_t_1)
    bound$L[taille] <- L_f
    bound$P[taille] <- P_f
  } else {
    print("augmenter le nombre de pas ou la limite en L")
  }
  bound
} #fin fonction

##### COURBE EQUIVALENTE AVEC SIGMOIDE
lambda = 1/18.0
L0_s <-g_v(y=P0,lambda);
lines(L0_s,P0, col="dark gray")
L_s = g_v(P_e,lambda)

bound_s = courbeInverse_s(L_s,P_e,VLmax,1000,50,lambda)
t_s = which(bound_s$P==0)
longueur = (if (is.na(t_s)) dim(bound_s)[1] else t_s)
bound_s = bound_s[1:longueur,]
#affichage
#lines(bound_s$L,bound_s$P,col="dark gray")

## sauvegarde courbes lambda = 1/18.0
eq_s=data.frame(L0=L0_s,P0=P0)
lamb = 18.0
#write.table(eq_s, file=paste0("eq_s",lamb,"u",round(VLmax,2),".csv"),row.names=FALSE, sep=";",dec=".")
#write.table(bound_s, file=paste0("bound_s",lamb,"u",round(VLmax,2),"P", round(P_e,2), ".csv"), row.names=FALSE, sep=";",dec=".")

################### FIN ##################

###################### IMPRESSION FIGURES ##################

########### scenario ############
# scenario 1 P_e = P0_2
P_e = P0_2
Pmax=P_e

##################################################
### FIGURE 5A MEMBER2 vs member 1
##################################################
# INIT Member 1
col1 = "dark gray"
L_e = f_v(P_e)

les_x = c(Lmin,Lmin,bound$L,Lmin)
les_y = c(0,Pmax,bound$P,0)
gymaxPt <- 30

# INIT Member 2 q dans [2.2,2.3] là (scenario 1) c'est 2.2 qui l'emporte
col2 = "gray"
le_q = 2.2
P_e = P0_2
L_e = f_v(P_e,le_q)
Pmax=P_e
bound_c = courbeInverse(L_e,P_e,VLmax,1000,50,le_q)
t_c = which(bound_c$P==0)
longueur = (if (is.na(t_c)) dim(bound_c)[1] else t_c)
bound_c = bound_c[1:longueur,]
les_x_2 = c(Lmin,Lmin,bound_c$L,Lmin)
les_y_2 = c(0,Pmax,bound_c$P,0)

plot(L0,P0,pch=" ",xlim=c(0,Lmax),ylim=c(0,gymaxPt), frame.plot=FALSE, xlab="L (µgl⁻¹)",ylab="P (µgl⁻¹)",   xaxs="i", yaxs="i", yaxt="n" );
#axis(1, pos=0)
axis(2, pos=0)
col1 = "dark gray"
col2 = "light gray"
polygon(les_x,les_y,col=col1)
polygon(les_x_2,les_y_2,col=col2)
lines(c(Lmin,Lmin,gymax),c(0,Pmax,Pmax),col=1); # contraintes

# legend
leg.txt <- c(paste("Member 1"), paste("Member 2"))
leg.col <- c(col1,col2)
legend(x=c(15),y=c(30),legend=leg.txt, fill=leg.col, text.col = "black", cex=0.9)

# Lmin et Pmax
leg1<-paste("Pmax=",round(Pmax,2));
leg2<-paste("Lmin");
text(c(7,8),c(1,Pmax+2),c(leg2,leg1),cex=1.0);
###############################################
########## END Figure 5A
################################################

##################################################
########## Figure 5B: Member 3 vs Member 1
##################################################
# INIT Member 1
col1 = "dark gray"
L_e = f_v(P_e)

les_x = c(Lmin,Lmin,bound$L,Lmin)
les_y = c(0,Pmax,bound$P,0)
gymaxPt <- 30
##### décommenter pour tracer member 1
#plot(L0,P0,pch=" ",xlim=c(0,Lmax),ylim=c(0,gymax), frame.plot=FALSE, xlab="L (µgl⁻¹)",ylab="P (µgl⁻¹)",   xaxs="i", yaxs="i", yaxt="n" );
#axis(2, pos=0)
#lines(L0,P0,lty=2) # trace les équilibres
# polygon(les_x,les_y,col=col1)
# lines(L0,P0,lty=2) # trace les équilibres
# lines(c(Lmin,Lmin,gymax),c(0,Pmax,Pmax),col=1); # contraintes


# INIT Member 3 
col3 = "gray"
le_q = q_b
le_b = 2.2
P_e = P0_2
L_e = f_v(P_e,le_q,le_b)
Pmax=P_e
bound_c = courbeInverse(L_e,P_e,VLmax,1000,50,le_q=q_b,le_b=le_b)
t_c = which(bound_c$P==0)
longueur = (if (is.na(t_c)) dim(bound_c)[1] else t_c)
bound_c = bound_c[1:longueur,]
les_x3 = c(Lmin,Lmin,bound_c$L,Lmin)
les_y3 = c(0,Pmax,bound_c$P,0)


### FIGURE
plot(L0,P0,pch=" ",xlim=c(0,Lmax),ylim=c(0,gymaxPt), frame.plot=FALSE, xlab="L (µgl⁻¹)",ylab="P (µgl⁻¹)",   xaxs="i", yaxs="i", yaxt="n" );
#axis(1, pos=0)
axis(2, pos=0)
col1 = "dark gray"
col2 = "light gray"
polygon(les_x,les_y,col=col1)
polygon(les_x3,les_y3,col=col2)
lines(c(Lmin,Lmin,gymax),c(0,Pmax,Pmax),col=1); # contraintes

# legend
leg.txt <- c(paste("Member 1"), paste("Member 3"))
leg.col <- c(col1,col2)
legend(x=c(15),y=c(30),legend=leg.txt, fill=leg.col, text.col = "black", cex=0.9)

# Lmin et Pmax
leg1<-paste("Pmax=",round(Pmax,2));
leg2<-paste("Lmin");
text(c(7,8),c(1,Pmax+2),c(leg2,leg1),cex=1.0);

###############################################
########## END Figure 5B
################################################

############################
### FIGURE 5C MEMBER 4 vs. 1 
############################
# INIT Member 1
col1 = "dark gray"
L_e = f_v(P_e)

les_x = c(Lmin,Lmin,bound$L,Lmin)
les_y = c(0,Pmax,bound$P,0)
gymaxPt <- 30

# INIT Member 4 lambda in [1/19,1/18] 
col4 = "light gray"
le_lambda = 1/19.0
le_b = b
P_e = P0_2
L_e = g_v(P_e,le_lambda,le_b)
Pmax=P_e
bound_s = courbeInverse_s(L_e,P_e,VLmax,1000,50,lambda = le_lambda,le_b=le_b)
t_s = which(bound_s$P==0)
longueur = (if (is.na(t_s)) dim(bound_s)[1] else t_s)
bound_s = bound_s[1:longueur,]
les_x4 = c(Lmin,Lmin,bound_s$L,Lmin)
les_y4 = c(0,Pmax,bound_s$P,0)

# figure
plot(L0,P0,pch=" ",xlim=c(0,Lmax),ylim=c(0,gymaxPt), frame.plot=FALSE, xlab="L (µgl⁻¹)",ylab="P (µgl⁻¹)",   xaxs="i", yaxs="i", yaxt="n" );
#axis(1, pos=0)
axis(2, pos=0)
col1 = "dark gray"
col4 = "light gray"
polygon(les_x,les_y,col=col1)
polygon(les_x4,les_y4,col=col4)
lines(c(Lmin,Lmin,gymax),c(0,Pmax,Pmax),col=1); # contraintes

# Member 1 en tranparence
lines(bound$L, bound$P, lty=2, col="black")
# 
# legend
leg.txt <- c(paste("Member 1"), paste("Member 4"))
leg.col <- c(col1,col2)
legend(x=c(15),y=c(30),legend=leg.txt, fill=leg.col, text.col = "black", cex=0.9)

# Lmin et Pmax
leg1<-paste("Pmax=",round(Pmax,2));
leg2<-paste("Lmin");
text(c(7,8),c(1,Pmax+2),c(leg2,leg1),cex=1.0);

###############################################
########## END Figure 5C
################################################

################################################
##### Figure 5d ####
################################################
#group guarantied viability kernel vs. Member 1
################################################

# scenario 1 P_e = P0_2
P_e = P0_2
Pmax=P_e

# INIT Member 1
col1 = "dark gray"
L_e = f_v(P_e)
les_x = c(Lmin,Lmin,bound$L,Lmin)
les_y = c(0,Pmax,bound$P,0)
gymaxPt <- 30


groupKernel <- read.table("bourget_C1T31bql_sc-viab-0.dat",
                          header = FALSE, 
                          colClasses="numeric")
names(groupKernel)<-c("L","P","v")
i = 0 # test
bound_g <- data.frame(L=0,P=0)
for (i in 0:999) {
  début <- i*1000+1
  fin <- i*1000+1000
  sousgroupKernel <- groupKernel[début:fin,]
  ind_p <-match(0,sousgroupKernel$v)
  le_p <- if (is.na(ind_p)) Pmax else {
    if (ind_p <= 1) 0 else sousgroupKernel$P[ind_p -1]}
  if (le_p!=0) {
    new_lp <- data.frame(L=sousgroupKernel$L[1],P=le_p)
    bound_g <- rbind(bound_g,new_lp)
  }
}
bound_g <- bound_g[-1,] #on enlève le 0,0 du début

# lines (bound_g$L,bound_g$P,col="red") # to see it

plot(L0,P0,pch=" ",xlim=c(0,Lmax),ylim=c(0,gymaxPt), frame.plot=FALSE, xlab="L (µgl⁻¹)",ylab="P (µgl⁻¹)",   xaxs="i", yaxs="i", yaxt="n" );
#axis(1, pos=0)
axis(2, pos=0)
col1 = "dark gray"
colg = "grey92"
polygon(les_x,les_y,col=col1)

les_xg = c(Lmin,Lmin,bound_g$L,bound_g$L[length(bound_g$L)],Lmin)
les_yg = c(0,Pmax,bound_g$P,0,0)


polygon(les_xg,les_yg,col=colg)
lines(c(Lmin,Lmin,gymax),c(0,Pmax,Pmax),col=1); # contraintes

# Member 1 en tranparence
# lines(bound$L, bound$P, lty=2, col="black")
# 
# legend
leg.txt <- c(paste("Member 1"), paste("Group"))
leg.col <- c(col1,colg)
legend(x=c(15),y=c(30),legend=leg.txt, fill=leg.col, text.col = "black", cex=0.9)

# Lmin et Pmax
leg1<-paste("Pmax=",round(Pmax,2));
leg2<-paste("Lmin");
text(c(7,8),c(1,Pmax+2),c(leg2,leg1),cex=1.0);
ligne= which(groupKernel$v==0)[1] # c'est la frontière pour le point 1

######################### END Impression Figures ############
############################################################

