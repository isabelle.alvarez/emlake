# emlake

Data references, code link and code files for the computation of guaranteed viability kernels in article “Collective management of environmental commons with multiple usages: a guaranteed viability approach”

## Summary
This directory contains: 

* The .r file which is used to produce Figure 5 in the paper. It should be run in RStudio or directly in R console.

* The .dat file which is the result of the computation of the group guaranteed viability kernel. It is used by the .r file to produce Figure 5(d).

* The .h files contains the implementation of the guaranteed viability problem for lake Bourget, to be used with the Viablab library to produce the .dat file.

* The .zip file contains the ViabLab library used to compute the .dat file, with instanciation from the .h files

## Data

The data used for Lake Bourget case are taken from:
[Brias et al., 2018] Brias, A., Mathias, J.-D., and Deffuant, G. (2018). Inter-
annual rainfall variability may foster lake regime shifts: An example from
Lake Bourget in France. Ecological Modelling, 389:11–18. 

## Viability library to run the .h files
https://github.com/lastre-viab/VIABLAB

ViabLab (from A. Désilles) is free to download, modify and run.
It has to be installed locally, using the following commands, after deleting file CMakeCache.txt if it already exists:
``` 
cmake .
make
./viabLabExe 
```

The .h files should to be added in the data directory if they are not, replacing the current lake_Bourget_C1T31bql_sc.h file.
Then (and at each modification of the .h file) the following commands must be run:
``` 
make
./viabLabExe 
```
The output .dat file can then be found in the OUTPUT directory.

In case of problem, make sure that in file /src/main.cpp
lake_Bourget_C1T31bql_sc.h is mentionned, otherwise remove the #include line and replace it by the following:
```
#include "../data/lake_Bourget_C1T31bql_sc.h"
```
**If you have problem with the github version, you can use the .zip file instead.**

## R library to execute .R file
https://cran.r-project.org/
